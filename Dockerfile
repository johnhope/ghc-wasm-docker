# syntax=docker/dockerfile:1
FROM ubuntu:22.04

# See https://gitlab.haskell.org/ghc/ghc-wasm-meta#building-guide

# Needed because GHC's utils/genprimopcode/Main.hs contains non-ASCII character
ENV LANG=C.UTF-8

# wasi-sdk requires cmake, ninja-build, clang, ccache, lld
# libffi-wasm requires clang-format
RUN apt-get update && apt-get install -y build-essential curl git libgmp-dev libnuma1 autoconf cmake ninja-build clang ccache lld clang-format jq unzip zstd

WORKDIR /work
RUN git clone --depth 1 https://gitlab.haskell.org/ghc/ghc-wasm-meta.git

WORKDIR /work/ghc-wasm-meta
RUN FLAVOUR=9.8 ./setup.sh

FROM ubuntu:22.04

COPY --from=0 /root/.ghc-wasm /root/.ghc-wasm

ENV PATH="${PATH}:/root/.ghc-wasm/wasm-run/bin"
ENV PATH="${PATH}:/root/.ghc-wasm/proot/bin"
ENV PATH="${PATH}:/root/.ghc-wasm/wasm32-wasi-cabal/bin"
ENV PATH="${PATH}:/root/.ghc-wasm/cabal/bin"
ENV PATH="${PATH}:/root/.ghc-wasm/wizer/bin"
ENV PATH="${PATH}:/root/.ghc-wasm/wazero/bin"
ENV PATH="${PATH}:/root/.ghc-wasm/wasmedge/bin"
ENV PATH="${PATH}:/root/.ghc-wasm/wasmtime/bin"
ENV PATH="${PATH}:/root/.ghc-wasm/wabt/bin"
ENV PATH="${PATH}:/root/.ghc-wasm/binaryen/bin"
ENV PATH="${PATH}:/root/.ghc-wasm/bun/bin"
ENV PATH="${PATH}:/root/.ghc-wasm/nodejs/bin"
ENV PATH="${PATH}:/root/.ghc-wasm/deno/bin"
ENV PATH="${PATH}:/root/.ghc-wasm/wasi-sdk/bin"
ENV PATH="${PATH}:/root/.ghc-wasm/wasm32-wasi-ghc/bin"

ENV AR="/root/.ghc-wasm/wasi-sdk/bin/llvm-ar"
ENV CC="/root/.ghc-wasm/wasi-sdk/bin/clang"
ENV CC_FOR_BUILD="/root/.ghc-wasm/wasi-sdk"
ENV CXX="/root/.ghc-wasm/wasi-sdk/bin/clang++"
ENV LD="/root/.ghc-wasm/wasi-sdk/bin/wasm-ld"
ENV NM="/root/.ghc-wasm/wasi-sdk/bin/llvm-nm"
ENV OBJCOPY="/root/.ghc-wasm/wasi-sdk/bin/llvm-objcopy"
ENV OBJDUMP="/root/.ghc-wasm/wasi-sdk/bin/llvm-objdump"
ENV RANLIB="/root/.ghc-wasm/wasi-sdk/bin/llvm-ranlib"
ENV SIZE="/root/.ghc-wasm/wasi-sdk/bin/llvm-size"
ENV STRINGS="/root/.ghc-wasm/wasi-sdk/bin/llvm-strings"
ENV STRIP="/root/.ghc-wasm/wasi-sdk/bin/llvm-strip"
ENV LLC="/bin/false"
ENV OPT="/bin/false"

ENV CONF_CC_OPTS_STAGE2=${CONF_CC_OPTS_STAGE2:-"-Wno-error=int-conversion -Wno-error=strict-prototypes -Wno-error=implicit-function-declaration -Oz -msimd128 -mnontrapping-fptoint -msign-ext -mbulk-memory -mmutable-globals -mmultivalue -mreference-types"}
ENV CONF_CXX_OPTS_STAGE2=${CONF_CXX_OPTS_STAGE2:-"-Wno-error=int-conversion -Wno-error=strict-prototypes -Wno-error=implicit-function-declaration -fno-exceptions -Oz -msimd128 -mnontrapping-fptoint -msign-ext -mbulk-memory -mmutable-globals -mmultivalue -mreference-types"}
ENV CONF_GCC_LINKER_OPTS_STAGE2=${CONF_GCC_LINKER_OPTS_STAGE2:-"-Wl,--compress-relocations,--error-limit=0,--growable-table,--stack-first,--strip-debug "}
ENV CONF_CC_OPTS_STAGE1=${CONF_CC_OPTS_STAGE1:-"-Wno-error=int-conversion -Wno-error=strict-prototypes -Wno-error=implicit-function-declaration -Oz -msimd128 -mnontrapping-fptoint -msign-ext -mbulk-memory -mmutable-globals -mmultivalue -mreference-types"}
ENV CONF_CXX_OPTS_STAGE1=${CONF_CXX_OPTS_STAGE1:-"-Wno-error=int-conversion -Wno-error=strict-prototypes -Wno-error=implicit-function-declaration -fno-exceptions -Oz -msimd128 -mnontrapping-fptoint -msign-ext -mbulk-memory -mmutable-globals -mmultivalue -mreference-types"}
ENV CONF_GCC_LINKER_OPTS_STAGE1=${CONF_GCC_LINKER_OPTS_STAGE1:-"-Wl,--compress-relocations,--error-limit=0,--growable-table,--stack-first,--strip-debug "}
ENV CONFIGURE_ARGS=${CONFIGURE_ARGS:-"--host=x86_64-linux --target=wasm32-wasi --with-intree-gmp --with-system-libffi"}
ENV CROSS_EMULATOR=${CROSS_EMULATOR:-"'"$PREFIX/wasm-run/bin/wasmtime.sh"'"}

# Ensure cabal is up to date
RUN wasm32-wasi-cabal update && wasm32-wasi-cabal install alex happy

CMD ["wasm32-wasi-ghc"]
